import sys
sys.path.append("./build") # set to correct path for module

import chebypy

x = 1

x = chebypy.cheby_abcissas(a=-2, b=2, n=5)

w = chebypy.cheby_weights(x=1, a=-2, b=2, n=5, deriv=-1)

print(x)
print(w)
