#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <omp.h>

#include "chebyshev.hpp"

namespace py = pybind11;

using cx_double = std::complex<double>;
using std::vector;
using namespace chebyshev;

PYBIND11_MODULE(chebypy, m) {
    m.doc() = "Python interface for Chebyshev interpolation";

    m.def("cheby_weights", &chebyshev::cheby_weights, "weights for chebyshev",
          py::arg("x"), py::arg("a"), py::arg("b"), py::arg("n"), py::arg("deriv")=0, py::arg("m")=0);

    m.def("cheby_abcissas", &chebyshev::calc_cheby_abscissas, "abcissas for chebyshev",
          py::arg("a"), py::arg("b"), py::arg("n"));


}
