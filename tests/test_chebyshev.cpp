#define CATCH_CONFIG_MAIN
#include <vector>
#include <complex>


#include "catch.hpp"

#include "../chebyshev.hpp"



using namespace std;
using namespace std::complex_literals;
using dcomplex = complex<double>;
using namespace chebyshev;
using mgrid = std::vector<std::tuple<double, double, size_t>>;

template <typename T>
std::vector<T> linspace(T a, T b, size_t n) {
    if (n==0) return {};
    if (n==1) return {a};
    T h = (b - a) / static_cast<T>(n-1);
    std::vector<T> xs(n);
    for(size_t i=0;i<n;i++)
        xs[i]=a+i*h;
    return xs;
}


TEST_CASE( "chebyshev interpolation and integration", "[chebyshev]" )
{
    double abs_error = 1.0E-10;


    SECTION( "real function - check interpolation, integration and differentiation" ){
        double a = -2;
        double b = 2;
        int order = 50;
        int order_eval = 30;

        // f(x)
        auto func=[](double x) {
            return exp(-x * x);
        };

        // f'(x)
        auto dfunc=[](double x) {
            return - 2 * x * exp(-x * x);
        };

        // \int_a^b f(x) dx
        auto Ifunc=[](double aa, double bb) {
            return 0.5 * sqrt(M_PI) * (erf(bb) - erf(aa));
        };

        auto g = Chebyshev<double>(func, a, b, order, order_eval);
        auto dg = g.derivative();
        auto Ig = g.integral();

        int nn = 20;
        double dx = (b - a) / nn;
        for(int i=0; i<nn; i++){

            double x = a + dx * i;
            REQUIRE( abs(func(x) - g(x)) <= abs_error );
            REQUIRE( abs(dfunc(x) - dg(x)) <= abs_error );
            REQUIRE( abs(Ifunc(a, x) - Ig(x)) <= abs_error );
        }
    }

    SECTION( "complex function - check interpolation, integration and differentiation" ){
        double a = -2;
        double b = 2;
        int order = 50;
        int order_eval = 30;

        // f(x)
        auto func=[](double x) {
            return exp(1i * x);
        };

        // f'(x)
        auto dfunc=[](double x) {
            return 1i * exp(1i * x);
        };

        // \int_a^b f(x) dx
        auto Ifunc=[](double x) {
            return -1i * exp(1i * x);
        };

        auto g = Chebyshev<dcomplex>(func, a, b, order, order_eval);
        auto dg = g.derivative();
        auto Ig = g.integral();

        int nn = 20;
        double dx = (b - a) / nn;
        for(int i=0; i<nn; i++){

            double x = a + dx * i;

            REQUIRE( abs(func(x) - g(x)) <= abs_error );
            REQUIRE( abs(dfunc(x) - dg(x)) <= abs_error );
            REQUIRE( abs(Ifunc(x) - Ifunc(a) - Ig(x)) <= abs_error );
        }
    }

    SECTION( "complex function multi cheby" ){

        double abs_tol = 1E-14;

        mgrid grid;
        grid.push_back(make_tuple(-1, 2, 40));
        grid.push_back(make_tuple(2, 3, 20));

        auto xi = cheby_multi_xi(grid);
        auto func=[](double x) {return exp(-x * x) * (cos(x) + 1i * sin(x));};

        std::vector<dcomplex> fi;

        for (auto x : xi){
            fi.push_back(func(x));
        }

        auto g = Cheby<dcomplex>(fi, grid);

        int nn = 20;
        double dx = 4.0 / nn;
        for(int i=0; i<nn; i++){
            double x = -1 + dx * i;
            REQUIRE( abs(func(x) - g.eval(x)) <= abs_error );
        }
    }


    SECTION( "test full quadrature" ){

        int n = 40;

        double abs_tol = 1E-14;
        double rel_tol = 1E-14;

        auto func1=[](double x) {return 2 * exp(-x * x) / sqrt(M_PI);};
        // \int_a^b f(x) dx
        auto Ifunc=[](double aa, double bb) {return (erf(bb) - erf(aa));};

        double a=-3, b=2;

        // evaluate func1 on chebyshev abscissas
        auto x = calc_cheby_abscissas(a, b, n);
        std::vector<double> f1(n);
        for (uint i = 0; i < n; i++)
            f1[i] = func1(x[i]);

        // test the full integral
        auto w1 = cheby_weights(b, a, b, n, -1);
        double res1_ref = erf(b) - erf(a);
        double res1 = 0.;
        for(int i=0; i<n; i++)
            res1 += f1[i] * w1[i];
        REQUIRE( abs(res1 - res1_ref) <= abs_tol );
        REQUIRE( abs(res1 / res1_ref - 1.0) <= rel_tol );

        // test the function interpolation and the primitive
        int nn = 20;
        double dx = (b - a) / nn;
        for(int i=0; i<nn; i++){
            double x = a + dx * i;
            auto w0 = cheby_weights(x, a, b, n);
            auto wint = cheby_weights(x, a, b, n, -1);
            double res_int = 0., res_f = 0.;
            for(int j=0; j<n; j++){
                res_f += f1[j] * w0[j];
                res_int += f1[j] * wint[j];
            }
            REQUIRE( abs(res_int - Ifunc(a, x)) <= abs_tol );
            REQUIRE( abs(res_f - func1(x)) <= abs_tol );
        }

        auto func2=[](double x) {return sin(x);};

        // test the full integral
        auto xx = calc_cheby_abscissas(0, M_PI, n);
        std::vector<double> f2(n);
        for (uint i = 0; i < n; i++)
            f2[i] = func2(xx[i]);
        auto w2 = cheby_weights(M_PI, 0, M_PI, n, -1);
        double res2_ref = 2;
        double res2 = 0.;
        for(int i=0; i<n; i++)
            res2 += f2[i] * w2[i];
        REQUIRE( abs(res2 - res2_ref) <= abs_tol );
        REQUIRE( abs(res2 / res2_ref - 1.0) <= rel_tol );

    }

    SECTION( "test interval cheby" ){

        int n = 30;

        double abs_tol = 1E-14;

        auto func1=[](double x) {return 2 * exp(-x * x) / sqrt(M_PI);};

        double a=-3, b=2;

        auto cheby = Cheby<double>();

        // evaluate func1 on chebyshev abscissas
        auto x1 = calc_cheby_abscissas(a, (a+b)/2, n);
        std::vector<double> f1(n);
        for (uint i = 0; i < n; i++)
            f1[i] = func1(x1[i]);
        cheby.addInterval(f1, a, (a+b)/2);

        auto x2 = calc_cheby_abscissas((a+b)/2, b, n);
        std::vector<double> f2(n);
        for (uint i = 0; i < n; i++)
            f2[i] = func1(x2[i]);
        cheby.addInterval(f2, (a+b)/2, b);

        // test the function interpolation
        int nn = 20;
        double dx = (b - a) / nn;
        for(int i=0; i<nn; i++){
            double x = a + dx * i;
            auto res = cheby.eval(x);
            REQUIRE( abs(res - func1(x)) <= abs_tol );
        }
    }


    SECTION( "test matrix cheby" ){

        double a = 0;
        double b = 2;
        int n = 40;

        // f(x)
        auto func=[](double x) -> arma::cx_mat {
            arma::cx_mat res(1, 1);
            res(0, 0) = exp(-x * x);
            return res;
        };

        // convolution integral \int_a^x dy exp(-y^2) * exp(-(y - x)^2)
        auto Conv=[](double a, double x) {
            return 0.5 * sqrt(M_PI / 2) * exp(- x * x / 2) * ( erf(x / sqrt(2)) - erf((2 * a - x) / sqrt(2)) );
        };

        // F(x) = \int_a^x dy exp(-y^2)
        auto F =[](double a, double x) {
            return 0.5 * sqrt(M_PI) * (erf(x) - erf(a));
        };

        std::vector<std::tuple<double, double, size_t>> grid;
        grid.push_back(make_tuple(a, b, n));

        auto g = ChebyMat<dcomplex>(grid);
        g.interpolate(func);

        int nn = 20;
        double dx = (b - a) / nn;

        // test evaluation
        for(int i=0; i<nn; i++){
            double x = a + dx * i;
            REQUIRE( abs(g.eval(x)(0, 0) - func(x)(0, 0)) <= abs_error );
        }

        // test convolution
        for(int i=0; i<nn; i++){
            double x = a + dx * i;
            REQUIRE( abs(g.convolute_left(func, x)(0, 0) - Conv(a, x)) <= abs_error );
        }

        // test inplace convolution
        auto g2 = ChebyMat<dcomplex>(grid);
        g2.interpolate(func);
        g2.convolute_inplace_left(func);
        for(int i=0; i<nn; i++){
            double x = a + dx * i;
            REQUIRE( abs(g2.eval(x)(0, 0) - Conv(a, x)) <= abs_error );
        }

        // test the primitive function
        g.integrate();
        for(int i=0; i<nn; i++){
            double x = a + dx * i;
            REQUIRE( abs(g.eval(x)(0, 0) - F(a, x)) <= abs_error );
        }
    }


}

