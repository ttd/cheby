#pragma once

#include <fstream>
#include <functional>
#include <iomanip>

using std::ostream;
using std::ifstream;
using std::function;
using std::setprecision;

namespace chebyshev{


template< typename S, typename T >
void write_sequence(const S & sequence, ostream& out, function<void(const T&, ostream&)> wfunc){
    out << sequence.size() << "\n";
    for (auto const& x:sequence)
        wfunc(x, out);
    out << "\n";
}

template< typename S, typename T >
S read_sequence(ifstream& in, function<T(ifstream&)> rfunc){
    int size;
    in >> size;
    S sequence;
    for (auto i=0; i < size; i++){
        auto elem = rfunc(in);
        sequence.push_back(elem);
    }
    return sequence;
}

template< typename T >
inline void saveScalar(const T& x, ostream& out){out << x << " ";};

template< typename T >
inline T readScalar(ifstream& in){T x; in >> x; return x;};

template< typename T >
inline void saveNumeric(const T& x, ostream& out){out << setprecision(18) << x << " ";};

}
